BUILD_PATH := vendor/apps/prebuilts

PRODUCT_SOONG_NAMESPACES += \
    vendor/apps/prebuilts

PRODUCT_PACKAGES += \
    dynamicSpotPrebuilt \
    GeminiPrebuilt \
    MiRemote \
    Photos \
    PixelWeather \
    Wallman \
    Via

ifeq ($(WITH_GMS),false)
PRODUCT_PACKAGES += \
    AuroraStorePrebuilt
endif
