#!/bin/bash

PRODUCT_BRAND=$(find $ANDROID_BUILD_TOP/vendor -name "*.mk" -exec grep -H "PRODUCT_BRAND" {} \; | awk -F= '{print $2}' | tr -d '[:space:]')

echo "PRODUCT_BRAND:$PRODUCT_BRAND"

# Assign the extracted value to PRODUCT_BRAND
export PRODUCT_BRAND=$PRODUCT_BRAND

# Backup the original Android.bp file
cp $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp Android.bp.orig

# Initialize a flag to track if modifications were made
MODIFIED=false

# Modify Android.bp based on PRODUCT_BRAND
if [[ $PRODUCT_BRAND == "risingOS" ]]; then
    if grep -q "Via" $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp; then
        echo "Android.bp was modified previously for RisingOS"
    else
        echo "Including RisingOS-specific apps"
        echo '

android_app_import {
    name: "Via",
    apk: "apps/ViaPrebuilt.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    privileged: false,
    product_specific: true,
    preprocessed: true,
}' >> $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp
        MODIFIED=true
    fi
fi

if [[ $PRODUCT_BRAND == "ProjectMatrixx" ]]; then
    if grep -q "Photos" $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp || grep -q "dynamicSpotPrebuilt" $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp; then
        echo "Android.bp was modified previously for ProjectMatrixx"
    else
        echo "Including Matrixx-specific apps"
        echo '

android_app_import {
    name: "Photos",
    owner: "gapps",
    apk: "apps/Photos.apk",
    overrides: ["Gallery2", "SnapdragonGallery", "Gallery", "Glimpse"],
    preprocessed: true,
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    product_specific: true,
}

android_app_import {
    name: "dynamicSpotPrebuilt",
    apk: "apps/dynamicSpotPrebuilt.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    privileged: false,
    product_specific: true,
    preprocessed: true,
}' >> $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp
        MODIFIED=true
    fi
fi

if [[ $PRODUCT_BRAND == "Alpha" ]]; then
    if grep -q "dynamicSpotPrebuilt" $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp; then
        echo "Android.bp was modified previously for Alpha"
    else
        echo "Including Alpha-specific apps"
        echo '

android_app_import {
    name: "dynamicSpotPrebuilt",
    apk: "apps/dynamicSpotPrebuilt.apk",
    presigned: true,
    dex_preopt: {
        enabled: false,
    },
    privileged: false,
    product_specific: true,
    preprocessed: true,
}' >> $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp
        MODIFIED=true
    fi
fi

# Verify the changes
diff Android.bp.orig $ANDROID_BUILD_TOP/vendor/apps/prebuilts/Android.bp > diff_output

if [[ $MODIFIED == true ]]; then
    echo "Android.bp modified successfully"
elif [[ -s diff_output ]]; then
    echo "Android.bp modified successfully"
else
    echo "Error: Android.bp file not modified"
fi

rm Android.bp.orig
rm diff_output
