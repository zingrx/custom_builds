### Prebuilt Apps Repo
You can add this prebuilt apps to any custom ROM on any device. It's adapted to add certain apps for certain ROMs.

```
Examples:
For RisingOS >> add Via browser as the ROM comes without any built-in browsing app
For Matrixx >> Photos is added as a replacement for built-in Gallery
```

Start by cloning this repo to your directory by either one of the two methods below:
1. Direct clone (this requires manual fetching of updates by `git pull` in the cloned folder)
   ```
   git clone https://gitlab.com/zingrx/vendor_apps_prebuilts vendor/apps/prebuilts
   ```
   
2. Add it to `.repo/local_manifest.xml` file (this method provides auto updates with `repo sync`). Create the file if it's not found.
   ```
   <remote name="zingrx-lab" fetch="https://gitlab.com/zingrx" />
   <project path="vendor/apps/prebuilts" name="vendor_apps_prebuilts" remote="zingrx-lab" revision="main" />
   ```
   Then run `repo sync --force-sync vendor/apps/prebuilts`.

Add the below line in your `device.mk` :
```
# Prebuilt Apps
$(call inherit-product-if-exists, vendor/apps/prebuilts/config.mk)
```
Then add/delete the desired apps in the `config.mk` file in this repo.

If you want to add the prebuilt apps as optional, you can use a flag like below:
- Step 1:
  In `device.mk`  add this
  ```
  # Prebuilt Apps
  ifeq ($(INCLUDE_PREBUILTS), true)
      $(call inherit-product-if-exists, vendor/apps/prebuilts/config.mk)
  endif
  ```
- Step 2:
  In `lineage_$device.mk`, add the below flag
  ```
  INCLUDE_PREBUILTS := true
  ```